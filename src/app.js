// Import Analytics Scripts
import {
  averageDogWalkTimeByBreed,
  walksBreedsDistribution,
  averageWalksRatingByBreed,
  walksPercentajeByBreed,
  averageDogAgeByBreed,
} from "./scripts/breed.script";

import {
  dogWalksByNeighborhoodGroup,
  averageWalkerRatingByNeighborhoodGroup,
  availableWalkersByNeighborhoodGroup,
  averageWalkTimeByNeighborhoodGroup,
  averageWalkCostByNeighborhoodGroup,
  walkerDistributionByNeighborhoodGroup,
  walkerWithoutWalksByNeighborhoodGroup,
  totalDogsWalkedByNeighborhoodGroup,
  averageDogAgeByNeighborhoodGroup,
  averageWalkerAgeByNeighborhoodGroup,
} from "./scripts/neighborhoodGroup.script";

import {
  ownersToWalkersRatioByCity,
  totalAmountOfWalksByCity,
  averageAmountOfFavoriteWalkersByCity,
  averageAmountOfOwnerDogsByCity,
  distributionOfDogsByCity,
  totalWalksByCity,
  totalAmountOfDogsByBreedAndCity,
} from "./scripts/city.script";

import { reviewsDataByWalker } from "./scripts/walker.script";

// Import Setup Script
import setup from "./setup";

// Import DB
import db from "./database/database";

// Sync db
const syncDB = async () => {
  try {
    // await db.sync({ force: true });
    // await setup();

    await db.sync();

    console.log("DB Connected");
  } catch (error) {
    console.log(error);
  }
};

export const executeAnalyticsPipeline = async () => {
  await syncDB();
  console.log("Pipeline Start ---------------------------");

  // Scripts
  // await averageDogWalkTimeByBreed();
  // await walksBreedsDistribution();
  // await averageWalksRatingByBreed();
  // await walksPercentajeByBreed();
  // await averageDogAgeByBreed();

  // await dogWalksByNeighborhoodGroup();
  // await averageWalkerRatingByNeighborhoodGroup();
  // await availableWalkersByNeighborhoodGroup();
  // await averageWalkTimeByNeighborhoodGroup();
  // await averageWalkCostByNeighborhoodGroup();
  // await walkerDistributionByNeighborhoodGroup();
  // await walkerWithoutWalksByNeighborhoodGroup();
  // await totalDogsWalkedByNeighborhoodGroup();
  // await averageDogAgeByNeighborhoodGroup();
  // await averageWalkerAgeByNeighborhoodGroup();

  // await ownersToWalkersRatioByCity();
  // await totalAmountOfWalksByCity();
  // await averageAmountOfFavoriteWalkersByCity();
  // await averageAmountOfOwnerDogsByCity();
  // await distributionOfDogsByCity();
  // await totalWalksByCity();
  // await totalAmountOfDogsByBreedAndCity();

  // await reviewsDataByWalker();

  await db.connectionManager.close();
  console.log("Pipeline End ---------------------------");
};
