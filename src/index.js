import "@babel/polyfill";
import {} from "dotenv/config";
import { executeAnalyticsPipeline } from "./app";

// Initialize App
(async () => {
  try {
    await executeAnalyticsPipeline();
  } catch (error) {
    console.log(error);
  }
})();
