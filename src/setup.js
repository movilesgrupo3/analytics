import faker from "faker";

import Breed from "./models/Breed";
import City from "./models/City";
import Dog from "./models/Dog";
import NeighborhoodGroup from "./models/NeighborhoodGroup";
import Owner from "./models/Owner";
import Report from "./models/Report";
import Walk from "./models/Walk";
import Walker from "./models/Walker";

const cities = [
  {
    name: "Bogotá",
    neighborhoodGroups: [
      { name: "Usaquén" },
      { name: "Chapinero" },
      { name: "Santa Fe" },
      { name: "San Cristóbal" },
      { name: "Usme" },
      { name: "Tunjuelito" },
      { name: "Bosa" },
      { name: "Kennedy" },
      { name: "Fontibón" },
      { name: "Engativá" },
    ],
  },
  {
    name: "Neiva",
    neighborhoodGroups: [
      { name: "Noroccidental" },
      { name: "Nororiental" },
      { name: "Entre Ríos" },
      { name: "Central" },
      { name: "Oriental" },
      { name: "Sur" },
      { name: "La Floresta" },
      { name: "Suroriental" },
      { name: "Norte" },
      { name: "Las Palmas" },
    ],
  },
];

const breeds = [
  { name: "Labrador Retriever" },
  { name: "French Bulldog" },
  { name: "German Shepherd Dog" },
  { name: "Golden Retriever" },
  { name: "Bulldog" },
  { name: "Poodle" },
  { name: "Beagle" },
  { name: "Rottweiler" },
  { name: "Boxer" },
  { name: "Samoyed" },
  { name: "Siberian Huskie" },
];

const setup = async () => {
  // Cities
  await setupCitiesAndNeighborhoodGroups();

  // Breeds
  await setupBreeds();

  // Users
  await setupUsers();

  // Dogs
  await setupDogs();

  // // Walks
  await setupWalks();

  // // Reports
  await setupReports();
};

// Cities and NeighborhoodGroups
const setupCitiesAndNeighborhoodGroups = async () => {
  try {
    await Promise.all(
      cities.map(async (city) => {
        const newCity = await City.create({
          name: city.name,
        });
        await Promise.all(
          city.neighborhoodGroups.map(async (neighborhoodGroup) => {
            await NeighborhoodGroup.create({
              name: neighborhoodGroup.name,
              cityId: newCity.id,
            });
          })
        );
      })
    );
  } catch (error) {
    console.log(error);
  }
};

// Breeds
const setupBreeds = async () => {
  try {
    await Promise.all(
      breeds.map(async (breed) => {
        await Breed.create({
          name: breed.name,
        });
      })
    );
  } catch (error) {
    console.log(error);
  }
};

// Users
const setupUsers = async () => {
  try {
    // Walkers
    for (let i = 0; i < faker.datatype.number({ min: 150, max: 350 }); i++) {
      await Walker.create({
        name: faker.name.firstName() + " " + faker.name.lastName(),
        email: faker.internet.email(),
        uid: faker.datatype.uuid(),
        identification: faker.datatype.uuid(),
        rating: faker.datatype.float({ min: 0, max: 5 }),
        imageURL: faker.image.imageUrl(),
        age: faker.datatype.number({ min: 18, max: 37 }),
      });
    }

    // Owners
    for (let i = 0; i < faker.datatype.number({ min: 300, max: 450 }); i++) {
      const neighborhoodGroups = await NeighborhoodGroup.findAll();
      const favoriteWalkers = await Walker.findAll();

      const newOwner = await Owner.create({
        name: faker.name.firstName() + " " + faker.name.lastName(),
        email: faker.internet.email(),
        uid: faker.datatype.uuid(),
        imageURL: faker.image.imageUrl(),
        neighborhoodGroupId:
          neighborhoodGroups[
            faker.datatype.number(neighborhoodGroups.length - 1)
          ].id,
      });
      const favoritesStart = faker.datatype.number(favoriteWalkers.length / 2);
      const favoritesEnd = faker.datatype.number({
        min: favoritesStart + 1,
        max: favoritesStart + faker.datatype.number({ min: 1, max: 10 }),
      });
      await newOwner.setFavoriteWalkers([
        ...favoriteWalkers.slice(
          favoritesStart,
          Math.min(favoritesEnd, favoriteWalkers.length - 1)
        ),
      ]);
    }
  } catch (error) {
    console.log(error);
  }
};

// Dogs
const setupDogs = async () => {
  try {
    const owners = await Owner.findAll();

    await Promise.all(
      owners.map(async (owner) => {
        const breeds = await Breed.findAll();
        for (let i = 0; i < faker.datatype.number({ min: 1, max: 3 }); i++) {
          await Dog.create({
            name: faker.name.firstName(),
            age: faker.datatype.number({ min: 1, max: 15 }),
            imageURL: faker.image.imageUrl(),
            ownerId: owner.id,
            breedId: breeds[faker.datatype.number(breeds.length - 1)].id,
          });
        }
      })
    );
  } catch (error) {
    console.log(error);
  }
};

// Walks
const setupWalks = async () => {
  try {
    const owners = await Owner.findAll();

    await Promise.all(
      owners.map(async (owner) => {
        const ownerDogs = await owner.getDogs();
        const walkers = await Walker.findAll();
        const neighborhoodGroups = await NeighborhoodGroup.findAll();

        if (ownerDogs.length) {
          const dog = ownerDogs[faker.datatype.number(ownerDogs.length - 1)];
          for (let i = 0; i < faker.datatype.number(50); i++) {
            await Walk.create({
              date: faker.date.past(),
              price: faker.datatype.float({ min: 0, max: 50000 }),
              distanceTraveled: faker.datatype.float({ min: 0, max: 2000 }),
              stepsWalked: faker.datatype.number(5),
              time: faker.datatype.float({ min: 0, max: 120 }),
              status: faker.datatype.number(3),
              dogId: dog.id,
              ownerId: owner.id,
              rating: faker.datatype.float({ min: 0, max: 5 }),
              walkerId:
                walkers[
                  faker.datatype.number({ min: 27, max: walkers.length - 1 })
                ].id,
              neighborhoodGroupId:
                neighborhoodGroups[
                  faker.datatype.number(neighborhoodGroups.length - 1)
                ].id,
            });
          }
        }
      })
    );
  } catch (error) {
    console.log(error);
  }
};

// Reports
const setupReports = async () => {
  try {
    const walks = await Walk.findAll();

    await Promise.all(
      walks.map(async (walk) => {
        if (faker.datatype.boolean()) {
          await Report.create({
            date: faker.date.past(),
            comment: faker.lorem.sentence(),
            rating: faker.datatype.float({ min: 0, max: 5 }),
            ownerId: walk.ownerId,
            walkerId: walk.walkerId,
            walkId: walk.id,
          });
        }
      })
    );
  } catch (error) {
    console.log(error);
  }
};

export default setup;
