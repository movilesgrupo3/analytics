import Sequelize from "sequelize";
import db from "../database/database";

import Walk from "./Walk";

const Dog = db.define("Dog", {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  age: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
    validate: {
      min: 0,
      max: 20,
    },
  },
  imageURL: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      isUrl: true,
    },
  },
});

// 1 to many association with Walk
Dog.hasMany(Walk, {
  onDelete: "CASCADE",
  foreignKey: "dogId",
});
Walk.belongsTo(Dog, {
  as: "dog",
  foreignKey: "dogId",
});

export default Dog;
