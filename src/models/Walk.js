import Sequelize from "sequelize";
import db from "../database/database";

import Report from "./Report";

const Walk = db.define("Walk", {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
  },
  date: {
    type: Sequelize.DATE,
    allowNull: false,
  },
  price: {
    type: Sequelize.DOUBLE,
    allowNull: false,
    defaultValue: 0,
    validate: {
      min: 0,
    },
  },
  distanceTraveled: {
    type: Sequelize.DOUBLE,
    allowNull: false,
    defaultValue: 0,
    validate: {
      min: 0,
    },
  },
  stepsWalked: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
    validate: {
      min: 0,
    },
  },
  time: {
    type: Sequelize.DOUBLE,
    allowNull: false,
    defaultValue: 0,
    validate: {
      min: 0,
    },
  },
  status: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
    validate: {
      min: 0,
      max: 3,
    },
  },
  rating: {
    type: Sequelize.DOUBLE,
    defaultValue: 0,
    validate: {
      min: 0,
      max: 5,
    },
  },
});

// Association with Report
Walk.hasOne(Report, {
  onDelete: "CASCADE",
  foreignKey: "walkId",
});
Report.belongsTo(Walk, {
  as: "walk",
  foreignKey: "walkId",
});

export default Walk;
