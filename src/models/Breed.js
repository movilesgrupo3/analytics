import Sequelize from "sequelize";
import db from "../database/database";

import Dog from "./Dog";

const Breed = db.define("Breed", {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
  },
});

// 1 to many association with Dog
Breed.hasMany(Dog, {
  onDelete: "CASCADE",
  foreignKey: "breedId",
});
Dog.belongsTo(Breed, {
  as: "breed",
  foreignKey: "breedId",
});

export default Breed;
