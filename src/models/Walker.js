import Sequelize from "sequelize";
import db from "../database/database";

import Report from "./Report";
import Walk from "./Walk";

const Walker = db.define("Walker", {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true,
    },
  },
  uid: {
    type: Sequelize.STRING,
    allowNull: true, // Deberia ser false
    unique: true,
  },
  identification: {
    type: Sequelize.STRING,
    unique: false,
  },
  rating: {
    type: Sequelize.DOUBLE,
    defaultValue: 0,
    validate: {
      min: 0,
      max: 5,
    },
  },
  imageURL: {
    type: Sequelize.STRING,
    validate: {
      isUrl: true,
    },
  },
  status: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  age: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
    validate: {
      min: 0,
      max: 60,
    },
  },
});

// 1 to many association with Report
Walker.hasMany(Report, {
  onDelete: "CASCADE",
  foreignKey: "walkerId",
});
Report.belongsTo(Walker, {
  as: "walker",
  foreignKey: "walkerId",
});

// 1 to many association with Walk
Walker.hasMany(Walk, {
  onDelete: "CASCADE",
  foreignKey: "walkerId",
});
Walk.belongsTo(Walker, {
  as: "walker",
  foreignKey: "walkerId",
});

export default Walker;
