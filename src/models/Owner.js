import Sequelize from "sequelize";
import db from "../database/database";

import Walker from "./Walker";
import Report from "./Report";
import Dog from "./Dog";
import Walk from "./Walk";

const Owner = db.define("Owner", {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true,
    },
  },
  uid: {
    type: Sequelize.STRING,
    allowNull: true, // Deberia ser false
    unique: true,
  },
  imageURL: {
    type: Sequelize.STRING,
    validate: {
      isUrl: true,
    },
  },
});

// many to many association with Walker
Walker.belongsToMany(Owner, {
  onDelete: "CASCADE",
  through: "OwnersFavoriteWalkers",
});
Owner.belongsToMany(Walker, {
  as: "favoriteWalkers",
  onDelete: "CASCADE",
  through: "OwnersFavoriteWalkers",
});

// 1 to many association with Report
Owner.hasMany(Report, {
  onDelete: "CASCADE",
  foreignKey: "ownerId",
});
Report.belongsTo(Owner, {
  as: "owner",
  foreignKey: "ownerId",
});

// 1 to many association with Dog
Owner.hasMany(Dog, {
  onDelete: "CASCADE",
  foreignKey: "ownerId",
});
Dog.belongsTo(Owner, {
  as: "owner",
  foreignKey: "ownerId",
});

// 1 to many association with Walk
Owner.hasMany(Walk, {
  onDelete: "CASCADE",
  foreignKey: "ownerId",
});
Walk.belongsTo(Owner, {
  as: "owner",
  foreignKey: "ownerId",
});

export default Owner;
