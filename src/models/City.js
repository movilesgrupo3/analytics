import Sequelize from "sequelize";
import db from "../database/database";

import NeighborhoodGroup from "./NeighborhoodGroup";

const City = db.define("City", {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
  },
});

// 1 to many association with NeighborhoodGroup
City.hasMany(NeighborhoodGroup, {
  onDelete: "CASCADE",
  foreignKey: "cityId",
});
NeighborhoodGroup.belongsTo(City, {
  as: "city",
  foreignKey: "cityId",
});

export default City;
