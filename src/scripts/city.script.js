import { QueryTypes } from "sequelize";
import db from "../database/database";
import axios from "axios";

// Business Question #19
export const ownersToWalkersRatioByCity = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      Select "WalkersQuery"."name", ("OwnersQuery"."Number_Of_Owners"*1.0/"WalkersQuery"."Number_Of_Walkers") AS "Owners_Walkers Ratio"
      FROM
      (SELECT "Cities"."name", COUNT(DISTINCT "Walkers"."id") AS "Number_Of_Walkers"
      FROM "Walks"
      INNER JOIN "Walkers" ON "Walks"."walkerId" = "Walkers"."id"
      INNER JOIN "NeighborhoodGroups" ON "Walks"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      GROUP BY "Cities"."name") AS "WalkersQuery"
      INNER JOIN
      (SELECT "Cities"."name", COUNT(DISTINCT "Owners"."id") AS "Number_Of_Owners"
      FROM "Owners"
      INNER JOIN "NeighborhoodGroups" ON "Owners"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      GROUP BY "Cities"."name") AS "OwnersQuery"
      ON "OwnersQuery"."name" = "WalkersQuery"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    console.log(query);

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${ownersToWalkersRatioByCity.name}.json`,
      { ...query }
    );

    console.log(`Processing ${ownersToWalkersRatioByCity.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #21
export const totalAmountOfWalksByCity = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name", COUNT("Walks"."id") AS "Total Amount of Walks"
      FROM "Cities" 
      INNER JOIN "NeighborhoodGroups" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      INNER JOIN "Walks" ON "NeighborhoodGroups"."id" = "Walks"."neighborhoodGroupId"
      GROUP BY "Cities"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${totalAmountOfWalksByCity.name}.json`,
      { ...query }
    );

    console.log(`Processing ${totalAmountOfWalksByCity.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #24
export const averageAmountOfFavoriteWalkersByCity = async () => {
  try {
    // SELECT "Cities"."name", (COUNT(DISTINCT "OwnersFavoriteWalkers"."WalkerId")*1.0/(SELECT COUNT(DISTINCT "OwnersFavoriteWalkers"."OwnerId") )) AS "Average Amount of Favorite Walkers"
    //   FROM "OwnersFavoriteWalkers"
    //   INNER JOIN "Owners" ON "Owners"."id" = "OwnersFavoriteWalkers"."OwnerId"
    //   INNER JOIN "NeighborhoodGroups" ON "NeighborhoodGroups"."id" = "Owners"."neighborhoodGroupId"
    //   INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
    //   GROUP BY "Cities"."name"

    // SELECT "Owners"."name", COUNT("OwnersFavoriteWalkers"."OwnerId")
    //   FROM "OwnersFavoriteWalkers"
    //   INNER JOIN "Owners" ON "Owners"."id" = "OwnersFavoriteWalkers"."OwnerId"
    //   GROUP BY "Owners"."name"
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name", (COUNT(DISTINCT "OwnersFavoriteWalkers"."WalkerId")*1.0/(SELECT COUNT(DISTINCT "OwnersFavoriteWalkers"."OwnerId") )) AS "Average Amount of Favorite Walkers"
      FROM "OwnersFavoriteWalkers"
      INNER JOIN "Owners" ON "Owners"."id" = "OwnersFavoriteWalkers"."OwnerId"
      INNER JOIN "NeighborhoodGroups" ON "NeighborhoodGroups"."id" = "Owners"."neighborhoodGroupId"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      GROUP BY "Cities"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );
    console.log(query);
    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${averageAmountOfFavoriteWalkersByCity.name}.json`,
      { ...query }
    );

    console.log(`Processing ${averageAmountOfFavoriteWalkersByCity.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #25
export const averageAmountOfOwnerDogsByCity = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name", COUNT(DISTINCT "Dogs"."id")*1.0/"CityOwnersQuery"."Total_Owners" AS "Average Amount of Dogs"
      FROM "Cities"
      INNER JOIN "NeighborhoodGroups" ON "NeighborhoodGroups"."cityId" = "Cities"."id"
      INNER JOIN "Owners" ON "Owners"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Dogs" ON "Dogs"."ownerId" = "Owners"."id"
      INNER JOIN
      (
        SELECT "Cities"."id", COUNT(DISTINCT "Owners"."id") AS "Total_Owners"
        FROM "Owners"
        INNER JOIN "NeighborhoodGroups" ON "NeighborhoodGroups"."id" = "Owners"."neighborhoodGroupId"
        INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
        GROUP BY "Cities"."id"
      ) AS "CityOwnersQuery" ON "Cities"."id" = "CityOwnersQuery"."id"
      GROUP BY "Cities"."name", "CityOwnersQuery"."Total_Owners"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${averageAmountOfOwnerDogsByCity.name}.json`,
      { ...query }
    );

    console.log(`Processing ${averageAmountOfOwnerDogsByCity.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #27
export const distributionOfDogsByCity = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name", (COUNT("Dogs"."id")*1.0/(SELECT COUNT(*) FROM "Walks")) AS "Dogs Percentaje"
      FROM "Cities" 
      INNER JOIN "NeighborhoodGroups" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      INNER JOIN "Walks" ON "NeighborhoodGroups"."id" = "Walks"."neighborhoodGroupId"
      INNER JOIN "Dogs" ON "Dogs"."id" = "Walks"."dogId"
      GROUP BY "Cities"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${distributionOfDogsByCity.name}.json`,
      { ...query }
    );

    console.log(`Processing ${distributionOfDogsByCity.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #30
export const totalWalksByCity = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name" AS "City", COUNT("Walks"."id") AS "Total amount of walks" 
      FROM "Walks"
      INNER JOIN "NeighborhoodGroups" ON "Walks"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      GROUP BY "Cities"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${totalWalksByCity.name}.json`,
      { ...query }
    );

    console.log(`Processing ${totalWalksByCity.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #32
export const totalAmountOfDogsByBreedAndCity = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name" AS "City", "Breeds"."name" AS "Breed", COUNT("Dogs"."id") AS "Total Amount of Dogs"
      FROM "Cities" 
      INNER JOIN "NeighborhoodGroups" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      INNER JOIN "Walks" ON "NeighborhoodGroups"."id" = "Walks"."neighborhoodGroupId"
      INNER JOIN "Dogs" ON "Dogs"."id" = "Walks"."dogId"
      INNER JOIN "Breeds" ON "Breeds"."id" = "Dogs"."breedId"
      GROUP BY "Cities"."name", "Breeds"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    console.log(query);

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${totalAmountOfDogsByBreedAndCity.name}.json`,
      { ...query }
    );

    console.log(`Processing ${totalAmountOfDogsByBreedAndCity.name}`);
  } catch (error) {
    console.log(error);
  }
};
