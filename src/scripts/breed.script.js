import { QueryTypes } from "sequelize";
import db from "../database/database";
import axios from "axios";

// Business Question #10
export const averageDogWalkTimeByBreed = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Breeds"."name" AS "Breed Name", AVG("Walks"."time") AS "Average Time"
      FROM "Walks" 
      INNER JOIN "Dogs" ON "Walks"."dogId" = "Dogs"."id"
      INNER JOIN "Breeds" ON "Dogs"."breedId" = "Breeds"."id"
      GROUP BY "Breeds"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${averageDogWalkTimeByBreed.name}.json`,
      { ...query }
    );

    console.log(`Processing ${averageDogWalkTimeByBreed.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #15
export const walksBreedsDistribution = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Breeds"."name", COUNT(*) AS "Count"
      FROM "Walks" 
      INNER JOIN "Dogs" ON "Walks"."dogId" = "Dogs"."id"
      INNER JOIN "Breeds" ON "Dogs"."breedId" = "Breeds"."id"
      GROUP BY "Breeds"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${walksBreedsDistribution.name}.json`,
      { ...query }
    );

    console.log(`Processing ${walksBreedsDistribution.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #18
export const averageWalksRatingByBreed = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Breeds"."name" AS "Breed Name", AVG("Reports"."rating") AS "Average Rating"
      FROM "Walks" 
      INNER JOIN "Reports" ON "Walks"."id" = "Reports"."walkId"
      INNER JOIN "Dogs" ON "Walks"."dogId" = "Dogs"."id"
      INNER JOIN "Breeds" ON "Dogs"."breedId" = "Breeds"."id"
      GROUP BY "Breeds"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${averageWalksRatingByBreed.name}.json`,
      { ...query }
    );

    console.log(`Processing ${averageWalksRatingByBreed.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #20
export const walksPercentajeByBreed = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Breeds"."name" AS "Breed Name", (COUNT("Walks"."id")*100 / (SELECT COUNT(*) FROM "Walks")) AS "Walks Percentaje"
      FROM "Walks" 
      INNER JOIN "Dogs" ON "Walks"."dogId" = "Dogs"."id"
      INNER JOIN "Breeds" ON "Dogs"."breedId" = "Breeds"."id"
      GROUP BY "Breeds"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${walksPercentajeByBreed.name}.json`,
      { ...query }
    );

    console.log(`Processing ${walksPercentajeByBreed.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #23
export const averageDogAgeByBreed = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Breeds"."name" AS "Breed Name", AVG("Dogs"."age") AS "Average Dog Age"
      FROM "Walks" 
      INNER JOIN "Dogs" ON "Walks"."dogId" = "Dogs"."id"
      INNER JOIN "Breeds" ON "Dogs"."breedId" = "Breeds"."id"
      GROUP BY "Breeds"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${averageDogAgeByBreed.name}.json`,
      { ...query }
    );

    console.log(`Processing ${averageDogAgeByBreed.name}`);
  } catch (error) {
    console.log(error);
  }
};
