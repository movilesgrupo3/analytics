import { QueryTypes } from "sequelize";
import db from "../database/database";
import axios from "axios";

// Business Question #9
export const dogWalksByNeighborhoodGroup = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name" AS "City Name", "NeighborhoodGroups"."name" as "Neighborhood", count("Walks"."id") as "Number of Walks" 
      FROM "Walks" 
      INNER JOIN "NeighborhoodGroups" ON "Walks"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId" 
      WHERE "Walks"."date" <= (SELECT NOW()) AND "Walks"."date" >= (SELECT 'now'::timestamp - '1 month'::interval)
      GROUP BY "Cities"."name", "NeighborhoodGroups"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${dogWalksByNeighborhoodGroup.name}.json`,
      { ...query }
    );

    console.log(`Processing ${dogWalksByNeighborhoodGroup.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #11
export const averageWalkerRatingByNeighborhoodGroup = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name" AS "City Name", "NeighborhoodGroups"."name" AS "Neighborhood Group Name", AVG("Reports"."rating") AS "Average Walker Rating"
      FROM "Walks" 
      INNER JOIN "Reports" ON "Walks"."id" = "Reports"."walkId"
      INNER JOIN "NeighborhoodGroups" ON "Walks"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      GROUP BY "Cities"."name", "NeighborhoodGroups"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${averageWalkerRatingByNeighborhoodGroup.name}.json`,
      { ...query }
    );

    console.log(`Processing ${averageWalkerRatingByNeighborhoodGroup.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #12
export const availableWalkersByNeighborhoodGroup = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name" AS "City Name", "NeighborhoodGroups"."name" AS "Neighborhood Group", COUNT("Walks"."walkerId") AS "Walkers Available"
      FROM "Walks"
      INNER JOIN "Walkers" ON "Walks"."walkerId" = "Walkers"."id"
      INNER JOIN "NeighborhoodGroups" ON "Walks"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      GROUP BY "Cities"."name", "NeighborhoodGroups"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${availableWalkersByNeighborhoodGroup.name}.json`,
      { ...query }
    );

    console.log(`Processing ${availableWalkersByNeighborhoodGroup.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #13
export const averageWalkTimeByNeighborhoodGroup = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name" AS "City Name", "NeighborhoodGroups"."name" AS "Neighborhood Group Name", AVG("Walks"."time") AS "Average Walk Time"
      FROM "Walks" 
      INNER JOIN "Reports" ON "Walks"."id" = "Reports"."walkId"
      INNER JOIN "NeighborhoodGroups" ON "Walks"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      GROUP BY "Cities"."name", "NeighborhoodGroups"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${averageWalkTimeByNeighborhoodGroup.name}.json`,
      { ...query }
    );

    console.log(`Processing ${averageWalkTimeByNeighborhoodGroup.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #14
export const averageWalkCostByNeighborhoodGroup = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name" AS "City Name", "NeighborhoodGroups"."name" AS "Neighborhood Group Name", AVG("Walks"."price") AS "Average Walk Price"
      FROM "Walks" 
      INNER JOIN "Reports" ON "Walks"."id" = "Reports"."walkId"
      INNER JOIN "NeighborhoodGroups" ON "Walks"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      GROUP BY "Cities"."name", "NeighborhoodGroups"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${averageWalkCostByNeighborhoodGroup.name}.json`,
      { ...query }
    );

    console.log(`Processing ${averageWalkCostByNeighborhoodGroup.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #16
export const walkerDistributionByNeighborhoodGroup = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name" AS "City Name", "NeighborhoodGroups"."name" AS "Neighborhood Group Name", COUNT("Walkers"."id") AS "Walker Count"
      FROM "Walks" 
      INNER JOIN "Reports" ON "Walks"."id" = "Reports"."walkId"
      INNER JOIN "Walkers" ON "Walks"."walkerId" = "Walkers"."id"
      INNER JOIN "NeighborhoodGroups" ON "Walks"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      GROUP BY "Cities"."name", "NeighborhoodGroups"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${walkerDistributionByNeighborhoodGroup.name}.json`,
      { ...query }
    );

    console.log(`Processing ${walkerDistributionByNeighborhoodGroup.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #17 - Toca cambiar el 1 por un 0
export const walkerWithoutWalksByNeighborhoodGroup = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name" AS "City Name", "NeighborhoodGroups"."name" AS "Neighborhood Group Name", COUNT("Consulta"."id") AS "Walker Count"
      FROM "Walks"
      INNER JOIN "Reports" ON "Walks"."id" = "Reports"."walkId"
      INNER JOIN "Walkers" ON "Walks"."walkerId" = "Walkers"."id"
      INNER JOIN "NeighborhoodGroups" ON "Walks"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      INNER JOIN 
      (SELECT "Walkers"."id", COUNT("Walks"."id") AS Cuenta
        FROM "Walks" 
        INNER JOIN "Reports" ON "Walks"."id" = "Reports"."walkId"
        INNER JOIN "Walkers" ON "Walks"."walkerId" = "Walkers"."id"
        GROUP BY "Walkers"."id"
        HAVING COUNT("Walks"."id") = 4) AS "Consulta" ON "Walkers"."id" = "Consulta"."id"
      GROUP BY "Cities"."name", "NeighborhoodGroups"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${walkerWithoutWalksByNeighborhoodGroup.name}.json`,
      { ...query }
    );

    console.log(`Processing ${walkerWithoutWalksByNeighborhoodGroup.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #29
export const totalDogsWalkedByNeighborhoodGroup = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name" AS "City Name", "NeighborhoodGroups"."name" AS "Neighborhood Group", COUNT(DISTINCT "Walks"."dogId") AS "Dogs Walked"
      FROM "Walks"
      INNER JOIN "NeighborhoodGroups" ON "Walks"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      GROUP BY "Cities"."name", "NeighborhoodGroups"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${totalDogsWalkedByNeighborhoodGroup.name}.json`,
      { ...query }
    );

    console.log(`Processing ${totalDogsWalkedByNeighborhoodGroup.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #31
export const averageDogAgeByNeighborhoodGroup = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name" AS "City Name", "NeighborhoodGroups"."name" AS "Neighborhood Group", AVG("Dogs"."age") AS "Average Dog Age"
      FROM "Walks"
      INNER JOIN "Dogs" ON "Walks"."dogId" = "Dogs"."id"
      INNER JOIN "NeighborhoodGroups" ON "Walks"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      GROUP BY "Cities"."name", "NeighborhoodGroups"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    console.log(query);

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${averageDogAgeByNeighborhoodGroup.name}.json`,
      { ...query }
    );

    console.log(`Processing ${averageDogAgeByNeighborhoodGroup.name}`);
  } catch (error) {
    console.log(error);
  }
};

// Business Question #35
export const averageWalkerAgeByNeighborhoodGroup = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Cities"."name" AS "City Name", "NeighborhoodGroups"."name" AS "Neighborhood Group", AVG("Walkers"."age") AS "Average Walker Age"
      FROM "Walks"
      INNER JOIN "Walkers" ON "Walks"."walkerId" = "Walkers"."id"
      INNER JOIN "NeighborhoodGroups" ON "Walks"."neighborhoodGroupId" = "NeighborhoodGroups"."id"
      INNER JOIN "Cities" ON "Cities"."id" = "NeighborhoodGroups"."cityId"
      GROUP BY "Cities"."name", "NeighborhoodGroups"."name"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    console.log(query);

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${averageWalkerAgeByNeighborhoodGroup.name}.json`,
      { ...query }
    );

    console.log(`Processing ${averageWalkerAgeByNeighborhoodGroup.name}`);
  } catch (error) {
    console.log(error);
  }
};
