import { QueryTypes } from "sequelize";
import db from "../database/database";
import axios from "axios";

// Business Question #28
export const reviewsDataByWalker = async () => {
  try {
    // Query data
    const query = await db.query(
      `
      SELECT "Walkers"."name", "Walkers"."rating", "WalksData"."Total_Walks"
      FROM "Walkers"
      INNER JOIN
      (SELECT "Walkers"."id", COUNT("Walkers"."id") AS "Total_Walks"
      FROM "Walkers" 
      INNER JOIN "Walks" ON "Walks"."walkerId" = "Walkers"."id"
      GROUP BY "Walkers"."id") AS "WalksData" ON "Walkers"."id" = "WalksData"."id"
      GROUP BY "Walkers"."name", "Walkers"."rating", "WalksData"."Total_Walks"
      `,
      {
        type: QueryTypes.SELECT,
      }
    );

    await axios.put(
      `https://canem-app-default-rtdb.firebaseio.com/${reviewsDataByWalker.name}.json`,
      { ...query }
    );

    console.log(`Processing ${reviewsDataByWalker.name}`);
  } catch (error) {
    console.log(error);
  }
};
